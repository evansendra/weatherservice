/**
 * 
 */
package weather;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Timer;
import java.util.TimerTask;

import observer.Driver;
import observer.WeatherSubject;

/**
 * @author jayurbain
 * Date: 1/4/2014
 * 
 * Weather Service API
 *
 */

public class WeatherService {
	
	// the weather subject to update every TIME_INTERVAL seconds
	private WeatherSubject subject;
	
	private Timer t;
	
	// time between updates (in ms)
	
	private String units; // units to use for queries
	private String ndays; // number of days to be forecast
	
	private final static String WEATHER_COM_URL      = 
			"http://wxdata.weather.com/wxdata/weather/local/%s?unit=%s&dayf=%s&cc=*";
	
	private final static String LOCID_SEARCH_URL     = 
			"http://wxdata.weather.com/wxdata/search/search?where=%s";
	
	private final static Charset UTF8_CHARSET = Charset.forName("UTF-8");

	
	public WeatherService(WeatherSubject subject, String units, String ndays)
	{
		this.subject = subject;
		this.units = units;
		this.ndays = ndays;
	}
	
	String decodeUTF8(byte[] bytes) {
		return new String(bytes, UTF8_CHARSET);
	}

	static byte[] encodeUTF8(String string) {
		return string.getBytes(UTF8_CHARSET);
	}

	/**
	 * Request weather from weather.com
	 * @param locationId - A five digit US zip code or location ID. 
	 * 	To find your location ID, use function getLocationFromWeatherDotCom().
	 * @param units - WeatherDataGeneratorSimulator reference
	 * 
	 * Note: choosing metric units changes all the weather units to metric.
	 * For example, wind speed will be reported as kilometers per hour and
	 * barometric pressure as millibars.
	 * 
	 * @return weather_data that exists in XML feed.
	 */
	public static String weatherFromWeatherDotCom(String locationId, String units, String ndays) {
				
		// http://wxdata.weather.com/wxdata/weather/local/53217?&unit=m&dayf=5&cc=*
		String query = String.format( WEATHER_COM_URL, locationId, units, ndays );
		return urlQuery( query );
	}
	
	/**
	 * Get location IDs for place names matching a specified string from weather.com
	 * @param query - Plaintext string to match to available place names.
	 * For example, 'Los Angeles' will return matches for the city of that name in 
	 * California, Chile, Cuba, Nicaragua, etc as well as 'East Los Angeles, CA', 
	 * 'Lake Los Angeles, CA', etc.
	 * 
	 * @return loc_id_data: A dictionary of tuples in the following format:
	 * 	{'count': 2, 0: (LOCID1, Placename1), 1: (LOCID2, Placename2)}
	 * @throws UnsupportedEncodingException 
	 */
	public static String getLocationIds(String query) throws UnsupportedEncodingException {
				
		// "http://wxdata.weather.com/wxdata/search/search?where=Bayside%20WI
		// Note: Java uses UTF-16 by default, weather.com expects UTF-8
        // String q = new String( encodeUTF8(query) );
        String q = URLEncoder.encode(query, "UTF-8");
        String url = String.format( LOCID_SEARCH_URL, q );
		return urlQuery( url );
	}

	/**
	 * Submit URL query
	 * 
	 * @param well formed URL query, i.e., Convert spaces to +, etc. to make a valid URL
	 *   Use proper encoding: URLEncoder.encode(xxx, "UTF-8");
	 * @return URL response String
	 * @throws Exception 
	 */
	public static String urlQuery(String query) {

		System.out.println(" Querying " + query);
		String response = "";
		
		try {
			// Assume: query = URLEncoder.encode(query, "UTF-8");
			URL url = new URL(query);
			URLConnection connection = url.openConnection();
			//connection.addRequestProperty("Referer", HTTP_REFERER);

			// Get the XML response
			String line;
			StringBuilder builder = new StringBuilder();
			BufferedReader reader = new BufferedReader( new InputStreamReader(connection.getInputStream()) );
			while((line = reader.readLine()) != null) {
				builder.append(line);
			}

			response = builder.toString();
//			System.out.println(response);
		}
		catch (Exception e) {
			System.err.println("Something went wrong in urlQuery ...");
			e.printStackTrace();
		}		
		return response;
	}
	
	/**
	 * this method queries the weather service every TIME_INTERVAL seconds
	 * to retrieve a forecast; it updates the WeatherSubject upon successful
	 * query
	 */
	public void run()
	{
		t = new Timer();
		t.scheduleAtFixedRate
		(
			new TimerTask()
			{
				public void run()
				{
					String forecastXMLString = WeatherService.weatherFromWeatherDotCom(subject.getLocation().getId(), units, ndays);
					// System.out.println( forecastXMLString );		
					WeatherDotComForecastXMLParser xmlForecatParser;
					try 
					{
						xmlForecatParser = new WeatherDotComForecastXMLParser();
						xmlForecatParser.parseXML( forecastXMLString );
						// List<WeatherDotComForecast> forecastList = xmlForecatParser.getForecastList();
						subject.setForecasts(xmlForecatParser.getForecastList());
					}
					catch (Exception e) 
					{
						synchronized(WeatherService.class)
						{
							System.out.printf("Unable to update forecast for %s at this time\n", subject.getLocation());
						}
					}
				}
			}, 
			new Long(0),  // run immediately first time through
			Driver.UPDATE_DELAY  // how long to wait recurringly
			);
	}
	
	public WeatherSubject getWeatherSubject()
	{
		return subject;
	}
	
	public void stop()
	{
		t.cancel();
	}

}