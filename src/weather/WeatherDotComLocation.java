/*
 * @author Jay Urbain
 *
 * Wrapper for weather.com location
 * 
 * Date: 1/5/2014
 */

package weather;

public class WeatherDotComLocation {

	String id; // weather.com location id
	String type; // ??
	String location; // Readable location string
	
	/**
	 * @param id 		// weather.com location id
	 * @param type 		// ??
	 * @param loation 	// readable location string
	 */
	public WeatherDotComLocation(String id, String type, String location) {
		super();
		this.id = id;
		this.type = type;
		this.location = location;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	public String toString() {
		return id +"|"+type+"|"+location;
	}
}
