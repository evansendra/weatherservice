/*
 * @author Jay Urbain
 *
 * Wrapper for weather.com location
 * 
 * Date: 1/5/2014
 */

package weather;

public class WeatherDotComForecast {

	String id; 		// id
	String location; // readable location string
	String time; 	// readable time string
	String wind; 	// wind magnitude
	String day; 	// readable day string, e.g., Sunday
	String date; 	// readable date string
	String temp; 	// current temp
	String hiTemp; 	// hi temp
	String lowTemp; // low temp

	/**
	 * @param loation
	 *            // readable location string
	 * @param time
	 *            // readable time string
	 * @param wind
	 *            // wind magnitude
	 * @param day
	 *            // readable day string, e.g., Sunday
	 * @param date
	 *            // readable date string
	 * @param hiTemp
	 *            // hi temp
	 * @param lowTemp
	 *            // low temp
	 */
	public WeatherDotComForecast(String id, String location, String time,
			String wind, String day, String date, String temp, String hiTemp, String lowTemp) {
		super();
		this.id = id;
		this.location = location;
		this.time = time;
		this.wind = wind;
		this.day = day;
		this.date = date;
		this.temp = temp;
		this.hiTemp = hiTemp;
		this.lowTemp = lowTemp;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLoation() {
		return location;
	}

	public void setLoation(String location) {
		this.location = location;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getWind() {
		return wind;
	}

	public void setWind(String wind) {
		this.wind = wind;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	public String getTemp() {
		return temp;
	}
	
	public void setTemp(String temp) {
		this.temp = temp;
	}	

	public String getHiTemp() {
		return hiTemp;
	}

	public void setHiTemp(String hiTemp) {
		this.hiTemp = hiTemp;
	}

	public String getLowTemp() {
		return lowTemp;
	}

	public void setLowTemp(String lowTemp) {
		this.lowTemp = lowTemp;
	}

	public String toString() {
		return id + "|" + location + "|" + time + "|" + wind + "|" + day + "|"
				+ date + "|" + temp + "|" + hiTemp + "|" + lowTemp;
	}
}
