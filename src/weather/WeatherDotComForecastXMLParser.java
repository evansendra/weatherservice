package weather;

/*
 * @author Jay Urbain
 *
 * XML SAX Parser for weather.com locations
 * 
 * Inputs: XML file:
	See example:
	http://wxdata.weather.com/wxdata/weather/local/USWI0455?unit=e&dayf=5&cc=*
	
	Usage example:
	WeatherDotComLocationXMLParser x = new WeatherDotComLocationXMLParser();
	x,parse( xmlString);
	List<WeatherDotComLocation> list = x.getList();
 * 
 * Date: 1/5/2014
 */

import java.io.*;
import java.sql.*;
import java.util.*;

import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;


public class WeatherDotComForecastXMLParser extends DefaultHandler {
	
	String elementName;
	Attributes attributes;
	String chars;
	
	 String id; 	// id
	 String location; 	// readable location string
	 String time; 		// readable time string
	 String wind; 		// wind magnitude
	 String day;		// 1, 2, ...
	 String date; 		// readable date string
	 String temp; 		// current temp
	 String hiTemp; 	// hi temp
	 String lowTemp; 	// low temp
	List<WeatherDotComForecast> forecastList;
	
	/*
 	Querying http://wxdata.weather.com/wxdata/search/search?where=Milwaukee
	<weather ver="2.0"><head>      <locale>en_US</locale>      <form>MEDIUM</form>      <ut>F</ut>      <ud>mi</ud>      <us>mph</us>      <up>in</up>      <ur>in</ur>    </head>        <loc id="USWI0455">      <dnam>Milwaukee, WI</dnam>      <tm>7:45 AM</tm>      <lat>43.04</lat>      <lon>-87.91</lon>      <sunr>7:23 AM</sunr>      <suns>4:31 PM</suns>      <zone>-6</zone>    </loc>                <swa>        <a id="148871" uc="1">        <t>Wind Chill Warning until Tue Jan 07 2014 12:00 PM</t>        <l>http://www.weather.com/weather/alerts/localalerts/USWI0455?phenomena=WC&amp;significance=W&amp;areaid=WIZ066&amp;office=KMKX&amp;etn=0001</l>      </a>      </swa>        <cc>        <lsup>1/05/14 7:25 AM CST</lsup>        <obst>West Allis, WI, US</obst>        <tmp>12</tmp>        <flik>-2</flik>        <t>Cloudy</t>        <icon>26</icon>                <bar>          <r>30.09</r>          <d>steady</d>        </bar>        <wind>      <s>12</s>      <gust>23</gust>      <d>0</d>      <t>N</t>    </wind>        <hmid>56</hmid>        <vis>10.0</vis>        <uv>          <i>0</i>          <t>Low</t>        </uv>        <dewp>1</dewp>        <moon>          <icon>4</icon>          <t>Waxing Crescent</t>        </moon>      </cc>        <dayf>        <lsup>1/05/14 7:45 AM CST</lsup>        <day d="0" t="Sunday" dt="Jan 5">        <hi>12</hi>        <low>-19</low>        <sunr>7:23 AM</sunr>        <suns>4:31 PM</suns>                <part p="d">        <icon>14</icon>        <t>Snow Shower / Wind</t>        <wind>      <s>25</s>      <gust>N/A</gust>      <d>343</d>      <t>NNW</t>    </wind>        <bt>Snow Showers</bt>        <ppcp>40</ppcp>        <hmid>66</hmid>      </part>        <part p="n">        <icon>24</icon>        <t>Partly Cloudy / Wind</t>        <wind>      <s>26</s>      <gust>N/A</gust>      <d>310</d>      <t>NW</t>    </wind>        <bt>P Cldy/Wind</bt>        <ppcp>20</ppcp>        <hmid>64</hmid>      </part>      </day><day d="1" t="Monday" dt="Jan 6">        <hi>-13</hi>        <low>-17</low>        <sunr>7:23 AM</sunr>        <suns>4:31 PM</suns>                <part p="d">        <icon>24</icon>        <t>Mostly Cloudy / Wind</t>        <wind>      <s>26</s>      <gust>N/A</gust>      <d>284</d>      <t>WNW</t>    </wind>        <bt>M Cldy/Wind</bt>        <ppcp>10</ppcp>        <hmid>62</hmid>      </part>        <part p="n">        <icon>24</icon>        <t>Partly Cloudy / Wind</t>        <wind>      <s>23</s>      <gust>N/A</gust>      <d>265</d>      <t>W</t>    </wind>        <bt>P Cldy/Wind</bt>        <ppcp>10</ppcp>        <hmid>65</hmid>      </part>      </day><day d="2" t="Tuesday" dt="Jan 7">        <hi>1</hi>        <low>-7</low>        <sunr>7:23 AM</sunr>        <suns>4:31 PM</suns>                <part p="d">        <icon>30</icon>        <t>Partly Cloudy</t>        <wind>      <s>18</s>      <gust>N/A</gust>      <d>251</d>      <t>WSW</t>    </wind>        <bt>P Cloudy</bt>        <ppcp>10</ppcp>        <hmid>72</hmid>      </part>        <part p="n">        <icon>29</icon>        <t>Partly Cloudy</t>        <wind>      <s>7</s>      <gust>N/A</gust>      <d>241</d>      <t>WSW</t>    </wind>        <bt>P Cloudy</bt>        <ppcp>10</ppcp>        <hmid>77</hmid>      </part>      </day><day d="3" t="Wednesday" dt="Jan 8">        <hi>12</hi>        <low>9</low>        <sunr>7:23 AM</sunr>        <suns>4:31 PM</suns>                <part p="d">        <icon>26</icon>        <t>Cloudy</t>        <wind>      <s>6</s>      <gust>N/A</gust>      <d>181</d>      <t>S</t>    </wind>        <bt>Cloudy</bt>        <ppcp>20</ppcp>        <hmid>81</hmid>      </part>        <part p="n">        <icon>27</icon>        <t>Mostly Cloudy</t>        <wind>      <s>4</s>      <gust>N/A</gust>      <d>150</d>      <t>SSE</t>    </wind>        <bt>M Cloudy</bt>        <ppcp>20</ppcp>        <hmid>86</hmid>      </part>      </day><day d="4" t="Thursday" dt="Jan 9">        <hi>26</hi>        <low>22</low>        <sunr>7:23 AM</sunr>        <suns>4:31 PM</suns>                <part p="d">        <icon>28</icon>        <t>Mostly Cloudy</t>        <wind>      <s>8</s>      <gust>N/A</gust>      <d>209</d>      <t>SSW</t>    </wind>        <bt>M Cloudy</bt>        <ppcp>10</ppcp>        <hmid>85</hmid>      </part>        <part p="n">        <icon>26</icon>        <t>Cloudy</t>        <wind>      <s>7</s>      <gust>N/A</gust>      <d>222</d>      <t>SW</t>    </wind>        <bt>Cloudy</bt>        <ppcp>20</ppcp>        <hmid>84</hmid>      </part>      </day>      </dayf>                                                              </weather>
	*/
	
	public WeatherDotComForecastXMLParser() throws Exception {
		super();
	}

	//===========================================================
	// SAX DocumentHandler methods
	//===========================================================

	public void startDocument() throws SAXException {
		
		//System.out.println("START DOCUMENT File");
		forecastList = new ArrayList<WeatherDotComForecast>();
	}

	public void endDocument() throws SAXException {
		
	}

	public void startElement(String namespaceURI, String lName, // local name
			String qName, // qualified name
			Attributes attrs) throws SAXException {
		
		String eName = lName; // element name
		if ("".equals(eName))
			eName = qName; // namespaceAware = false
		elementName = eName;
		
		if (attrs != null) {
			attributes = attrs;
		}
		
		if(eName.equals("loc")) {
			id = attributes.getValue("id");
		}
		
		if(eName.equals("day")) {
			day = attributes.getValue("t");
			date = attributes.getValue("dt");
		}
		
		chars = "";
	}

	public void endElement(String namespaceURI, String sName, // simple name
			String qName // qualified name
			) throws SAXException {

		String eName = sName; // element name
		if ("".equals(eName))
			eName = qName; // namespaceAware = false
		
		if(eName.equals("dnam")) {
			location = chars;
		}

		if(eName.equals("tm")) {
			time = chars;
		}
		
		if(eName.equals("tmp")) {
			temp = chars;
		}
		
		if(eName.equals("s")) {
			wind = chars;
		}
				
		if(eName.equals("hi")) {
			hiTemp = chars;
		}
		
		if(eName.equals("low")) {
			lowTemp = chars;
		}			
		
		if(eName.equals("lsup")) {
			date = chars;
		}		
		
		if(eName.equals("day")) {

			WeatherDotComForecast forecast = new WeatherDotComForecast(id, location, time, wind,
					day, date, temp, hiTemp, lowTemp);
			forecastList.add(forecast);
		}
	}

	public void characters(char buf[], int offset, int len) throws SAXException {
		
		try {
			StringBuffer sb = new StringBuffer();
			for(int i=0; i<len; i++) {
				chars += buf[i+offset];
			}
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}
	
	public synchronized void parseXML(String xml) {

		// create file
//		String fname = "weather.xml";
//		File file = new File(fname);
//		file.delete();
//        try {
//        	BufferedWriter writer = new BufferedWriter(new FileWriter(fname, true));
//	        writer.write(queryFile);
//	        writer.newLine();   
//	        writer.close();
//        }
//        catch(IOException e) {
//        	System.out.println(e);
//        }
        
		// Use the default (non-validating) parser
		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setValidating(false);
		try {
			// Parse the input
			SAXParser saxParser = factory.newSAXParser();
			saxParser.parse(new InputSource(new StringReader(xml)), (DefaultHandler) this);
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	public List<WeatherDotComForecast> getForecastList() {
		return forecastList;
	}

	public void setForecastList(List<WeatherDotComForecast> forecastList) {
		this.forecastList = forecastList;
	}
}