package weather;

/*
 * @author Jay Urbain
 *
 * XML SAX Parser for weather.com locations
 * 
 * Inputs: XML file:
	<search ver="3.0"> 
	<loc id="USWI0455" type="1">Milwaukee, WI</loc>
	<loc id="USNC0452" type="1">Milwaukee, NC</loc>        
	</search>
	
	Example:
	WeatherDotComLocationXMLParser x = new WeatherDotComLocationXMLParser();
	x,parse( xmlString);
	List<WeatherDotComLocation> list = x.getList();
 * 
 * Date: 1/5/2014
 */

import java.io.*;
import java.sql.*;
import java.util.*;

import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;


public class WeatherDotComLocationXMLParser extends DefaultHandler {
	
	String elementName;
	Attributes attributes;
	String chars;
	
	String id;
	String type;
	String location;
	List<WeatherDotComLocation> locationList;
	
	/*
 	Querying http://wxdata.weather.com/wxdata/search/search?where=Milwaukee
	<search ver="3.0"> 
	<loc id="USWI0455" type="1">Milwaukee, WI</loc>
	<loc id="USNC0452" type="1">Milwaukee, NC</loc>        
	</search>
	*/
	
	public WeatherDotComLocationXMLParser() throws Exception {
		super();
	}

	//===========================================================
	// SAX DocumentHandler methods
	//===========================================================

	public void startDocument() throws SAXException {
		
		//System.out.println("START DOCUMENT File");
		locationList = new ArrayList<WeatherDotComLocation>();
	}

	public void endDocument() throws SAXException {
		
	}

	public void startElement(String namespaceURI, String lName, // local name
			String qName, // qualified name
			Attributes attrs) throws SAXException {
		
		String eName = lName; // element name
		if ("".equals(eName))
			eName = qName; // namespaceAware = false
		elementName = eName;
		
		if (attrs != null) {
			attributes = attrs;
		}
		
		if(eName.equals("loc")) {
			id = attributes.getValue("id");
			type = attributes.getValue("type");
		}
		
		chars = "";
	}

	public void endElement(String namespaceURI, String sName, // simple name
			String qName // qualified name
			) throws SAXException {

		String eName = sName; // element name
		if ("".equals(eName))
			eName = qName; // namespaceAware = false
		
		if(eName.equals("loc")) {
			location = chars;
			WeatherDotComLocation loc = new WeatherDotComLocation(id, type, location); 
			locationList.add(loc);
		}
	}

	public void characters(char buf[], int offset, int len) throws SAXException {
		
		try {
			StringBuffer sb = new StringBuffer();
			for(int i=0; i<len; i++) {
				chars += buf[i+offset];
			}
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}
	
	public synchronized void parseXML(String xml) {

		// create file
//		String fname = "weather.xml";
//		File file = new File(fname);
//		file.delete();
//        try {
//        	BufferedWriter writer = new BufferedWriter(new FileWriter(fname, true));
//	        writer.write(queryFile);
//	        writer.newLine();   
//	        writer.close();
//        }
//        catch(IOException e) {
//        	System.out.println(e);
//        }
        
		// Use the default (non-validating) parser
		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setValidating(false);
		try {
			// Parse the input
			SAXParser saxParser = factory.newSAXParser();
			saxParser.parse(new InputSource(new StringReader(xml)), (DefaultHandler) this);
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	public List<WeatherDotComLocation> getLocationList() {
		return locationList;
	}

	public void setLocationList(List<WeatherDotComLocation> locationList) {
		this.locationList = locationList;
	}

}