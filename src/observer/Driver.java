package observer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import weather.WeatherDotComLocation;
import weather.WeatherDotComLocationXMLParser;
import weather.WeatherService;

public class Driver 
{
	
	public final static long UPDATE_DELAY = 10000; 
	
	public static void main(String args[])
	{
		// TODO Auto-generated method stub
		try {
			
			// allow user to select units to use
			String units;
			do
			{
				System.out.println("Enter units to be used ('e' for english, 'm' for metric, 'exit' to exit):");
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				if((units = br.readLine()).length() == 0)
					System.exit(0);
			}
			while(!units.equalsIgnoreCase("e") && !units.equalsIgnoreCase("m"));
			
			// allow user to select number of days to be forecast
			String ndays = "0";
			int intDays = 0;
			boolean isInt = true;
			do
			{
				System.out.println("Enter the number of days for which you'd like a forecast (must be less than 6; return to exit):");
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				if((ndays = br.readLine().trim()).length() == 0)
					System.exit(0);
				
				try
				{
					intDays = Integer.parseInt(ndays);
				}
				catch(NumberFormatException nfe)
				{
					System.out.println("cauth");
					isInt = false;
				}
				
			}
			while(!isInt || intDays < 1 || intDays > 5);
			
			System.out.println("Enter location (or return to exit): ");

			//  open up standard input
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			String location = null;
			
			// used to manage all locations of the app
			LocationManager manager = LocationManager.getInstance();
			while( (location = br.readLine()).length() > 0) {

				String locationXMLString = WeatherService.getLocationIds( location );
				if(locationXMLString != null)
				{
					// System.out.println( locationXMLString );
					WeatherDotComLocationXMLParser xmlLocationParser = new WeatherDotComLocationXMLParser();
					xmlLocationParser.parseXML( locationXMLString );
					List<WeatherDotComLocation> locationList = xmlLocationParser.getLocationList();
					
					if(locationList.size() < 1)
						System.out.printf("Try another search; '%s' turned up no results\n", location);
					else if(locationList.size() > 1)
						System.out.printf("Try another search; '%s' turned up more than one result\n", location);
					else
					{
						System.out.printf("Number of locations: %s\n", locationList.size());
						System.out.println( locationList );
						for(WeatherDotComLocation loc : locationList) 
						{
							WeatherSubject curSubj = new WeatherSubject(loc);
							new WeatherObserver(curSubj);
							manager.addLocation(new WeatherService(curSubj, units, ndays));
						}
					}
				}
                System.out.println("Enter location (or return to exit): ");
			}
		}
		catch(IOException e) {
			e.printStackTrace();
			System.out.println(e);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		LocationManager.getInstance().freeMem();
	}
}