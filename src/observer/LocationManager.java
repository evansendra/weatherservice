package observer;

import java.util.LinkedList;
import java.util.List;

import weather.WeatherService;

/**
 * This class, a singleton, manages all the locations to be updated
 * for this application.  
 * @author sendrae
 */
public class LocationManager
{
	private static volatile LocationManager mngr;
	private List<WeatherService> locations;
	private Gui gui;
	
	/**
	 * singleton, instantiates attributes 
	 */
	public LocationManager() 
	{
		// gui = Gui.getInstance();
		locations = new LinkedList<WeatherService>();
	}
	
	/**
	 * returns the sole instance of this class, lazily.
	 * @return LocationManager
	 */
	public static LocationManager getInstance()
	{
		// double lock checking for thread safety
		if(mngr == null)
		{
			synchronized(LocationManager.class)
			{
				if(mngr == null)
				{
					mngr = new LocationManager();
				}
			}
		}
		return mngr;
	}
	
	/**
	 * adds a location to the manager and creates a thread from which
	 * the object will perform its tasks
	 * @param loc a WeatherService object
	 * @return boolean denoting success of addition to manager lists
	 */
	public boolean addLocation(WeatherService loc)
	{
		boolean ret = locations.add(loc);
		loc.run();
		gui.update(locations);
		return ret;
	}
	
	/**
	 * removes a location from the list and stops it from updating
	 * @param loc
	 * @return
	 */
	public boolean removeLocation(WeatherService loc)
	{
		loc.stop();
		gui.update(locations);
		return locations.remove(loc);
	}
	
	/**
	 * frees the memory occupied by threads and locations
	 * at end of program
	 */
	public void freeMem()
	{
		for(WeatherService ws: locations)
			ws.stop();
		locations = null;
		mngr = null;
		System.gc();
	}
}