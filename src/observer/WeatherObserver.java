package observer;

import java.util.List;

import weather.WeatherDotComForecast;

/**
 * This is the user interface of the program; it is the way in which the 
 * user asks the program for weather updates on specific locations
 * @author sendrae
 */
public class WeatherObserver implements Observer 
{
	private WeatherSubject subject;
	private List<WeatherDotComForecast> forecasts;
	
	public WeatherObserver(WeatherSubject subject)
	{
		this.subject = subject;
		subject.add(this, null);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void update(Object data) 
	{
		if(data == null)
			forecasts = subject.getForecasts();
		else
			forecasts = (List<WeatherDotComForecast>) data;
		
		// update forecasts in the console
		synchronized(this)
		{
			System.out.printf("## WEATHER UPDATE ##\n\n");
			for(WeatherDotComForecast f: forecasts)
			{
				System.out.printf
				(
                        "Forecast for %s as of %s:\n" + 
                                        "Date: %s\n" +
                                        "Actual Temp: %s\n" +
                                        "High: %s\n" +
                                        "Low: %s\n" +
                                        "Wind: %s\n"
                                        + "\n",
                                        f.getLoation(),
                                        f.getTime(),
                                        f.getDate(),
                                        f.getTemp(),
                                        f.getHiTemp(),
                                        f.getLowTemp(),
                                        f.getWind()
                        );
			}
		}
	}
	
}