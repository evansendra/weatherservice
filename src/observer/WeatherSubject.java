package observer;

import java.util.LinkedList;
import java.util.List;

import weather.WeatherDotComForecast;
import weather.WeatherDotComLocation;

public class WeatherSubject implements Subject 
{
	private List<Observer> observers;
	private List<WeatherDotComForecast> forecasts;
	private WeatherDotComLocation loc;
	
	public WeatherSubject(WeatherDotComLocation loc)
	{
		this.loc = loc;
		observers = new LinkedList<Observer>();
	}

	@Override
	public void add(Observer o, String request) 
	{
		observers.add(o);
	}

	@Override
	public void remove(Observer o) 
	{
		observers.remove(o);
	}

	@Override
	public void notifyObservers() 
	{
		for(Observer o: observers)
			o.update(forecasts);
	}
	
	public WeatherDotComLocation getLocation()
	{
		return loc;
	}

	public List<WeatherDotComForecast> getForecasts() 
	{
		return forecasts;
	}
	
	public void setForecasts(List<WeatherDotComForecast> forecasts)
	{
		this.forecasts = forecasts;
		notifyObservers();
	}
}