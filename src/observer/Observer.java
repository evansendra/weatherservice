package observer;

/**
 * @author urbain
 * @data 3/23/2010
 *
 */

public interface Observer {
	public void update(Object data);
}
