package observer;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import weather.WeatherService;

/**
 * instantiates and provides the GUI for this application
 * @author sendrae
 *
 */
public class Gui extends JFrame implements Observer 
{
	private static final Dimension DIM = Toolkit.getDefaultToolkit().getScreenSize();
	private static final int CENTER_X = (int) DIM.getWidth() / 2;
	private static final int CENTER_Y = (int) DIM.getHeight() / 2;
	private List<String> locations = new ArrayList<String>();
	private String[] locationsArr = {""};
	private static String[] units = {"English", "Metric"};
	// private String[] locations = {""};
	// private static Gui gui = new Gui(); // lazy instantiation
	private LocationManager locManager;
	
	public Gui()
	{
		locManager = LocationManager.getInstance();
		
		// hide the inbox window
		setVisible(false);

		setMinimumSize(new Dimension(700, 400));
		setLocation(CENTER_X - getWidth() / 2, CENTER_Y - getHeight() / 2);
		setResizable(true);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setTitle("Weather Viewer");
		setLayout(new GridBagLayout());

		// list components
		/*
		JLabel toLbl = new JLabel("To:");
		JLabel fromLbl = new JLabel("From:");
		JLabel subjectLbl = new JLabel("Subject:");
		JLabel attachLbl = new JLabel("Attachment: ");
		JLabel messageLbl = new JLabel("Message:");
		*/
		
		JLabel viewLocationLbl = new JLabel("View Location:");
		JLabel addLocationLbl = new JLabel("Add Location");
		JLabel removeLocationLbl = new JLabel("Remove Location");
		JLabel unitsLbl = new JLabel("Units:");
		JLabel noWeatherLbl = new JLabel("Add a location to see forecasts!");
		

		/*
		final JTextField toField = new JTextField();
		JLabel from = new JLabel(sender);
		final JTextField subjectField = new JTextField();
		final JLabel attachFileLbl = new JLabel("<none>");
		final JTextArea messageField = new JTextArea();
		messageField.setLineWrap(true);
		final JScrollPane scrollingMessageField = new JScrollPane(messageField);
		scrollingMessageField.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollingMessageField.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		messageField.setBorder(BorderFactory.createLineBorder(Color.black));
		*/
		JComboBox locationList = new JComboBox(locationsArr);
		locationList.setSelectedIndex(0);
		
		JPanel locationPnl = new JPanel(new BorderLayout());
		locationPnl.add(viewLocationLbl, BorderLayout.NORTH);
		locationPnl.add(locationList, BorderLayout.SOUTH);
		
		JComboBox unitList = new JComboBox(units);
		unitList.setSelectedIndex(0);
		
		JPanel unitPnl = new JPanel(new BorderLayout());
		unitPnl.add(unitsLbl, BorderLayout.NORTH);
		unitPnl.add(unitList, BorderLayout.SOUTH);
		
		JButton addBtn = new JButton(addLocationLbl.getText());
		JButton removeBtn = new JButton(removeLocationLbl.getText());
		
		JPanel addRemovePnl = new JPanel(new BorderLayout());
		addRemovePnl.add(addBtn, BorderLayout.NORTH);
		addRemovePnl.add(removeBtn, BorderLayout.SOUTH);

		/*
		JButton sendBtn = new JButton("Send");
		JButton backBtn = new JButton("Back to Inbox");
		JButton attachBtn = new JButton("Attach a File");
		*/
		
		// layout the components on the gridbag
		GridBagConstraints c = new GridBagConstraints();
		// default padding between components
		int ypad = 5;
		int xpad = 5;

		c.insets = new Insets(5, 5, 5, 5); // top right bottom left
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.weighty = 0.2;
		c.weightx = 0.25;
		c.gridx = 0;
		c.gridy = 0;
		c.ipadx = xpad;
		c.ipady = ypad;
		// composeFrame.add(toLbl, c);
		add(locationPnl, c);
		
		
		c.gridx = 1;
		add(addRemovePnl, c);
		
		c.gridx = 2;
		c.gridy = 0;
		add(unitPnl, c);
		
		c.gridx = 0;
		c.gridy = 1;
		c.weightx = 1.0;
		c.weighty = 0.8;
		add(noWeatherLbl, c);

		// make the compose email window viewable
		setVisible(true);

		/*
		// add action listeners to buttons
		backBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				composeFrame.dispose();
				// make the main inbox viewable again
				setVisible(true);
			}
		});

		attachBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				// int returnVal = chooser.showOpenDialog();
				int retVal = chooser.showOpenDialog(composeFrame);
				if (retVal == JFileChooser.APPROVE_OPTION) {
					sendAttachment = true;
					attachedFile = chooser.getSelectedFile();

					attachFileLbl.setText(attachedFile.getName());
				}

			}
		});

		// make the send button aggregate and send the message
		sendBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				receiver = toField.getText().trim();
				if (receiver.isEmpty()) {
					JOptionPane.showMessageDialog(composeFrame, "Receiver field cannot be empty.", "Oops!", JOptionPane.WARNING_MESSAGE);
				} else {
					subject = subjectField.getText().trim();
					message = messageField.getText().trim();

					try {
						sendEmail(receiver);
						composeFrame.dispose();
						setVisible(true);
						JOptionPane.showMessageDialog(null, "Message Sent!", "Confirmation", JOptionPane.INFORMATION_MESSAGE);
					} catch (Exception e1) {
						JOptionPane.showMessageDialog(composeFrame, "Email could not be sent at this time.", "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});	
		*/
	}
	
	/*public static Gui getInstance()
	{
		return gui;
	}*/
	
	@Override
	public void update(Object data)
	{
		// TODO Auto-generated method stub
		locations.clear();
		List<WeatherService> newLocations = (List<WeatherService>) data;
		for(WeatherService ws: newLocations)
		{
			locations.add(ws.getWeatherSubject().getLocation().getLocation());
		}
		locations.add(0, "");
		locationsArr = (String[])locations.toArray();
	}

	public static void main(String args[])
	{
		new Gui();
	}
}