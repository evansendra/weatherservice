package observer;

/**
 * @author urbain
 * @data 3/23/2010
 *
 */

public interface Subject {
	public void add(Observer o, String request);
	public void remove (Observer o);
	public void notifyObservers();
}
